#!/bin/bash

. /home/gjbigs/functions.sh

ztmp() {
    declare zpath="/sys/block/zram1";
    declare z0="/sys/block/zram0";
    declare zdev="/dev/zram1";
    declare mkx4=(mkfs.ext4);
    declare mkbtr=(mkfs.btrfs);
    declare mou=(mount -o rw,strictatime,noexec,nosuid,nodev);
    declare cmod=(chmod 1777 -R)
    declare tmp="/tmp";
    if [[ -d $z0 ]]; then
        cat /sys/class/zram-control/hot_add;
        for I in "$zpath"; do
            wr zstd "$I"/comp_algorithm;
            wr 8GiB "$I"/disksize;
        done;
        for I in "$zdev"; do
            $mkx4 "$I";
	    #$mkf2 "$I";
	    #$mkbtr "$I";
	    sleep 3;
#            $mou "$I" "$tmp" && ${cmod[@]} "$tmp";
#            mount -o rw,strictatime,noexec,nosuid,nodev /dev/zram1 /tmp;
            #mount -o \
            #rw,relatime,noexec,nosuid,nodev,compress_chksum,atgc,gc_merge,lazytime,nodiscard,fsync_mode=nobarrier /dev/zram1 /tmp
	    mount -o rw,strictatime,noexec,nosuid,nodev /dev/zram1 /tmp;
	    chmod 1777 -R /tmp && chmod 1777 -R /tmp/*;
        done;
    else
        exit 1;
    fi;
}

ztmp;

exit 0
