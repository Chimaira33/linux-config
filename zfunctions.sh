#!/bin/zsh

rnice() {
	if [[ "$(id -u)" -eq 0 ]]; then
		declare rni
		rni=(renice -n $1)
	else
		declare rni
		rni=(sudo renice -n $1)
	fi
	$rni "$(pgrep "$2")"
}

boost() {
	sudo /usr/lib/booster/regenerate_images
}

reshiny() {
	if [[ "$(id -u)" -eq 0 ]]; then
		declare shi=(shiny-mirrors refresh -f medium -M transfer)
	else
		declare shi=(sudo shiny-mirrors refresh -f medium -M transfer)
	fi
	$shi
}

visedit() {
	if [[ "$(id -u)" -eq 0 ]]; then
		declare vdo=(visudo -f /etc/sudoers.d/01-custom)
	else
		declare vdo=(sudo visudo -f /etc/sudoers.d/01-custom)
	fi
	EDITOR=lvim
	$vdo
}

vsdo() {
	if [[ "$(id -u)" -eq 0 ]]; then
		declare vsd=(visudo)
	else
		declare vsd=(sudo visudo)
	fi
	$vsd
}

ytd() {
	yt-dlp "$1" -f bv*[height <=?1080][vcodec~='^((he|a)vc|h26[45])']+ba/b
}

ffd() {
	fd -u -t f "$@"
}

dfd() {
	fd -u -t d "$@"
}

rpsync() {
	repo sync "$1" --force-sync \
		--no-tags \
		--no-clone-bundle
}

mkgrub() {
	if [[ "$(id -u)" -eq 0 ]]; then
		declare mkg=(grub-mkconfig -o /boot/grub/grub.cfg)
	else
		declare mkg=(sudo grub-mkconfig -o /boot/grub/grub.cfg)
	fi
	$mkg
}

rsy() {
	rsync -rpElog "$@"
}

pacexp() {
	if [[ "$(id -u)" -eq 0 ]]; then
		declare setex=(pacman -D --asexplicit)
	else
		declare setex=(sudo pacman -D --asexplicit)
	fi
	$setex "$@"
}

scat() {
	declare scatt=(sudo cat)
	$scatt "$1"
}

rmd() {
	declare rmd=(rm -rf)
	$rmd "$@"
}

schedb() {
	declare schb=(schedtool -B -e)
	$schb "$@"
}

bootupdate() {
	declare sucp=(sudo cp)
	$sucp /boot/*.img \
		/efi/
	$sucp /boot/vmlinuz-linux \
		/efi/
}

mkinit() {
	declare sumkin=(sudo mkinitcpio -p)
	declare mkin=(mkinitcpio -p)
	if [[ "$(id -u)" -eq 0 ]]; then
		$mkin "$1"
	else
		$sumkin "$1"
	fi
}

delpaclk() {
	declare surm=(sudo rm)
	if [[ -e /var/lib/pacman/db.lck ]]; then
		$surm /var/lib/pacman/db.lck
	fi
}

pln() {
	printf '%s\n' "$*"
}

wr() {
	pln() {
		printf '%s\n' "$*"
	}
	if [[ ! -e "$2" ]]; then
		pln "$2 Doesn't Exist. Skipping."
		return 1
	fi
	declare chk
	chk="$(cat "$2" </dev/null 2>/dev/null)"
	if [[ $chk = "$1" ]]; then
		pln "$2 is already set to $1. Skipping."
		return 1
	fi
	if ! pln "$1" >"$2" </dev/null 2>/dev/null; then
		pln "Error When Writing To $2. Skipping."
		return 1
	fi
}

paci() {
	declare spcu=(sudo pacman -U)
	declare pcu=(pacman -U)
	if [[ "$(id -u)" -eq 0 ]]; then
		$pcu *.zst
		#$pcu *.bz2
	else
		$spcu *.zst
	fi
}

pstper() {
	declare per=(sudo pstate-frequency -S -g performance)
	$per
}

pstpow() {
	declare pow=(sudo pstate-frequency -S -g powersave)
	$pow
}

mkpkg() {
	declare mkg=(makepkg -sc --needed --noconfirm)
	if [[ "$(id -u)" -eq 0 ]]; then
		pln "Command can't be run as root. Exiting"
		return 1
	else
		$mkg
	fi
}

o3mkpkg() {
	declare mkg=(makepkg -sc --needed --noconfirm --config /etc/o3_makepkg.conf)
	$mkg "$@"
}

ofmkpkg() {
	declare mkg=(makepkg -sc --needed --noconfirm --config /etc/ofast_makepkg.conf)
	$mkg
}

qdisc() {
	#declare tcset=(tc qdisc replace root dev enp3s0 fq_codel)
	#	declare tcset2=(tc qdisc replace root dev enp3s0 fq_pie)
	declare tcset3=(tc qdisc replace root dev enp3s0 fq)
	$tcset3
	#$tcset
}

ctl() {

	declare curval
	curval="$(sysctl -e -n "$1")"

	if [[ -z $curval ]]; then
		pln "Key $1 does not exist. Skipping."
		return 1
	fi

	if [[ $curval = "$2" ]]; then
		pln "$1 is already set to $2. Skipping."
		return 0
	fi

	if [[ "$(id -u)" -eq 0 ]]; then
		declare wctl
		wctl=(sysctl -q -w)
	else
		declare wctl
		wctl=(sudo sysctl -q -w)
	fi
	$wctl "$1"="$2"
}

aurs() {
	declare auracs=(auracle --sort=votes search)
	$auracs "$@"
}

aurc() {
	declare aurac=(auracle -C /home/pkgs_gjbigs -r clone)
	declare cdpkg=(cd /home/pkgs_gjbigs)
	$aurac "$1"
	$cdpkg/$1
}

prepo() {
	declare pac=(paru --repo -G)
	declare cdpkg=(cd /home/pkgs_gjbigs)
	$cdpkg
	$pac $1
	$cdpkg/$1
}

gitc() {
	declare cdpkg=(cd /home/pkgs_gjbigs)
	declare ggitc=(git clone https://aur.archlinux.org)
	declare exll=(exa -l --color=auto)
	declare cdba=(cd ./)
	$cdpkg
	$ggitc/$1
	$cdba$1
	$exll
}

sbash() {
	if [[ ! -e "$1" ]]; then
		pln "Doesn't Exist."
		return 1
	else
		sudo bash "$1"
	fi
}
