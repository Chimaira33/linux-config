#!/bin/bash

. /home/gjbigs/functions.sh

zparam() {
	declare zpath="/sys/block/zram0"
	declare zdev="/dev/zram0"
	declare wrback="/dev/nvme1n1p6"
	declare mod=("modprobe")
	declare zct=("zramctl")
	declare zctp=("/dev/zram0 24GiB 8 zstd")
	zctl() {
		$zct "$1" -s "$2" -t "$3" -a "$4"
	}
	if [[ ! -d $zpath ]]; then
		$mod zram
	fi
	#for i in "$zpath"; do
	#    wr "$wrback" "$i"/backing_dev;
	#    wr huge "$i"/writeback;
	#done;
	zctl $zctp
}

mks() {
	declare M=("mkswap")
	for i in /dev/zram0; do
		$M "$i"
	done
}

sprio() {
	declare S=("swapon")
	$S "$@" -p 32000
}

swon() {
	for i in /dev/zram0; do
		sprio "$i"
	done
}

runz() {
	zparam
	mks
	swon
}

runz

exit 0
