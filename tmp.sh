#!/bin/bash

moutmp() {
    mount -O rw,noatime,noexec,nosuid,nodev,compress_chksum,atgc,gc_merge,background_gc=on,nodiscard,no_heap,inline_xattr,inline_data,inline_dentry,flush_merge,extent_cache,mode=adaptive,active_logs=6,alloc_mode=reuse,checkpoint_merge,fsync_mode=posix,compress_algorithm=zstd:6,compress_extension=*,compress_log_size=2,compress_mode=fs,discard_unit=block /dev/nvme1n1p6 /tmp;
}

moutmp;

exit 0
