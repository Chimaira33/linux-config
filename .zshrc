###
# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.
#installation via script from github
#export ZSH="/home/$USER/.oh-my-zsh"
#installation via paru -S oh-my-zsh-git
export ZSH=/usr/share/oh-my-zsh/

# Set name of the theme to load --- if set to "random", it will
# load a random theme each time oh-my-zsh is loaded, in which case,
# to know which specific one was loaded, run: echo $RANDOM_THEME
# See https://github.com/ohmyzsh/ohmyzsh/wiki/Themes
# if you installed the package oh-my-zsh-powerline-theme-git then you type here "powerline" as zsh theme
#ZSH_THEME="refined"

# Set list of themes to pick from when loading at random
# Setting this variable when ZSH_THEME=random will cause zsh to load
# a theme from this variable instead of looking in ~/.oh-my-zsh/themes/
# If set to an empty array, this variable will have no effect.

# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

# ZSH_THEME_RANDOM_IGNORED=(pygmalion tjkirch_mod)

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion.
# Case-sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to automatically update without prompting.
# DISABLE_UPDATE_PROMPT="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line if pasting URLs and other text is messed up.
# DISABLE_MAGIC_FUNCTIONS=true

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load?
# Standard plugins can be found in ~/.oh-my-zsh/plugins/*
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
#plugins=(git)

if [ -f $ZSH/oh-my-zsh.sh ]; then
	source $ZSH/oh-my-zsh.sh
fi

# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

#if [ -f /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh ]; then
#	source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
#fi

source /usr/share/zsh/plugins/fast-syntax-highlighting/fast-syntax-highlighting.plugin.zsh

autoload -U promptinit
promptinit
prompt pure

setopt GLOB_DOTS
#share commands between terminal instances or not
#unsetopt SHARE_HISTORY
setopt SHARE_HISTORY

#bindkey '\e[1~' beginning-of-line
#bindkey '\e[4~' end-of-line

if [[ $- != *i* ]]; then
	# Shell is non-interactive.  Be done now!
	return
fi

### Export ###
export HISTCONTROL=ignoreboth:erasedups
export PAGER='most'
export VISUAL=nvim
export EDITOR=nvim
export QT_STYLE_OVERRIDE=kvantum

if [[ -e $HOME/.android_sdk/cmdline-tools/latest/bin/sdkmanager ]]; then
	PATH="$HOME/.android_sdk/cmdline-tools/latest/bin:$HOME/.android_sdk/ndk-bundle:$HOME/.android_sdk/build-tools/33.0.0:$PATH"
	export ANDROID_HOME=~/.android_sdk
	export ANDROID_SDK_ROOT=~/.android_sdk
	export ANDROID_NDK_HOME=~/.android_sdk/ndk-bundle
fi

if [[ -e /usr/bin/paru ]]; then
	export PARU_PAGER=most
fi

if [[ -d ~/.cargo/bin ]]; then
	PATH="$HOME/.cargo/bin:$PATH"
fi

#if [[ -d "$HOME/.bin" ]]; then
#PATH="$HOME/.bin:$PATH"
#fi
#if [[ -d "$HOME/.local/bin" ]]; then
#	PATH="$HOME/.local/bin:$PATH"
#fi

if [[ -d "/home/gjbigs/.bin" ]]; then
	PATH="/home/gjbigs/.bin:$PATH"
fi
if [[ -d "/home/gjbigs/.local/bin" ]]; then
	PATH="/home/gjbigs/.local/bin:$PATH"
fi

### Aliases & Functions ###
#alias rg='rg -p'
alias grep='ag'
alias fgrep='fgrep --color=auto'
alias rmlogoutlock="sudo rm /tmp/arcologout.lock"
alias merge="xrdb -merge ~/.Xresources"
alias cleanup='sudo pacman -Rns $(pacman -Qtdq)'
ex() {
	if [ -f $1 ]; then
		case $1 in
		*.tar.bz2) tar xjf $1 ;;
		*.tar.gz) tar xzf $1 ;;
		*.bz2) bunzip2 $1 ;;
		*.rar) unrar x $1 ;;
		*.gz) gunzip $1 ;;
		*.tar) tar xf $1 ;;
		*.tbz2) tar xjf $1 ;;
		*.tgz) tar xzf $1 ;;
		*.zip) unzip $1 ;;
		*.Z) uncompress $1 ;;
		*.7z) 7z x $1 ;;
		*.deb) ar x $1 ;;
		*.tar.xz) tar xf $1 ;;
		*.tar.zst) tar xf $1 ;;
		*) echo "'$1' cannot be extracted via ex()" ;;
		esac
	else
		echo "'$1' is not a valid file"
	fi
}

setlvim() {
	alias vi='lvim'
	alias vim='lvim'
	alias svi='sudo lvim'
	export EDITOR='lvim'
	export VISUAL='lvim'
}
if [[ -e /home/gjbigs/.local/bin/lvim ]]; then
	setlvim
elif [[ -e /usr/bin/lvim ]]; then
	setlvim
else
	alias vi='nvim'
	alias vi='nvim'
	alias svi='sudo nvim'
fi
alias sct='sudo systemctl'
alias sno='sudo nano'
alias ls='exa --icons --color=auto'
alias ll='exa -l --octal-permissions --icons --color=auto'
alias la='exa -a --icons --color=auto'
alias lla='exa -la --octal-permissions --icons --color=auto'
alias lsd='exa -d --icons --color=auto'
alias lld='exa -ld --octal-permissions --icons --color=auto'
alias syncmirrors="sudo reflector -f 50 -c US -p https --sort rate --save /etc/pacman.d/mirrorlist"
if [[ -e /usr/bin/doas ]]; then
	alias sudo='doas '
else
	alias sudo='sudo '
fi
alias rdo='rdo '
alias kpi='killall picom'
alias jo='joshuto'
## NVM ##
snvm() {
	if [[ -e /usr/share/nvm/init-nvm.sh ]]; then
		source /usr/share/nvm/init-nvm.sh
	fi
}
. /home/gjbigs/zfunctions.sh

### Fetch ###
if [[ "$(id -u)" -ne 0 ]] && [[ -e /bin/fastfetch ]]; then
	sleep 0.05
	fastfetch
fi
