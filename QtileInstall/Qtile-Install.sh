#!/bin/bash

sudo pacman -S --needed sddm arcolinux-wallpapers-git thunar thunar-archive-plugin thunar-volman \
	arcolinux-xfce-git arcolinux-local-xfce4-git qtile sxhkd dmenu feh python-psutil xcb-util-cursor \
	arcolinux-qtile-git arcolinux-dconf-all-desktops-git arcolinux-config-all-desktops-git \
	awesome-terminal-fonts archlinux-logout-git python-setuptools unace unrar zip unzip p7zip pigz pixz pbzip2 \
	sharutils uudeview arj cabextract file-roller dconf-editor arc-gtk-theme arandr dmenu ntfs-3g \
	feh gmrun gtk-engine-murrine imagemagick lxappearance lxrandr picom playerctl android-tools \
	python-pywal volumeicon w3m urxvt-resize-font-git xfce4-notifyd rofi gvfs gvfs-mtp \
	xfce4-settings hardcode-fixer-git arcolinux-bin-git arcolinux-root-git arcolinux-termite-themes-git \
	archlinux-tweak-tool-git arcolinux-fonts-git awesome-terminal-fonts adobe-source-sans-fonts \
	cantarell-fonts noto-fonts ttf-bitstream-vera ttf-dejavu ttf-droid ttf-hack ttf-inconsolata \
	ttf-liberation ttf-roboto ttf-ubuntu-font-family tamsyn-font nvidia nvidia-settings alacritty

sudo hardcode-fixer

#cp -arf /etc/skel/. ~/

exit 0
