#!/bin/bash

. /home/gjbigs/functions.sh

hugeoff() {
	for i in /sys/kernel/mm/transparent_hugepage; do
		#wr never "$i"/defrag
		wr madvise "$i"/enabled
		#wr never "$i"/shmem_enabled
		#wr 0 "$i"/khugepaged/defrag
	done
}

iosch() {
	declare path=("/sys/block/nvme1n1/queue")
	# declare rkk=("read_ahead_kb")
	declare istat=("iostats")
	declare rot=("rotational")
	declare schd=("scheduler")

	for I in $path; do
		# wr 512 "$I"/$rkk
		wr 0 "$I"/$istat
		wr 0 "$I"/$rot
		wr none "$I"/$schd
	done
}

eeeoff() {
	ethtool \
		--set-eee \
		enp3s0 \
		eee \
		off
}

setcustom() {
	declare trac="/sys/kernel/tracing/tracing_on"
	declare kprob="/sys/kernel/debug/kprobes/enabled"
	for I in /sys/devices/system/cpu/intel_pstate; do
		wr 1 "$I"/hwp_dynamic_boost
		#wr 100 "$I"/min_perf_pct
	done
	wr 0 "$trac"
	wr 0 "$kprob"
}

setgov() {
	if [[ -d /sys/devices/system/cpu/cpufreq/policy0 ]]; then
		for I in /sys/devices/system/cpu/cpufreq/policy{0..7}; do
			wr performance "$I"/scaling_governor
		done
	fi
}

schedtweak() {
	for I in /sys/kernel/debug/sched; do
		wr 0 "$I"/tunable_scaling
		#wr 128 "$I"/nr_migrate
		#wr NEXT_BUDDY "$I"/features
		#wr NO_NONTASK_CAPACITY "$I"/features
	done
}

sleep 20
setcustom
hugeoff
iosch
schedtweak
ctl kernel.sched_energy_aware 0
zen
disidle
qdisc
eeeoff

exit 0
