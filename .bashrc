if [[ $- != *i* ]]; then
	# Shell is non-interactive.  Be done now!
	return
fi

[[ $DISPLAY ]] && shopt -s checkwinsize

#PROMPT_DIRTRIM=2
#PS1='\[\e[0;32m\]\w\[\e[0m\] \[\e[0;97m\]\$\[\e[0m\] '
PS1='[\u@\h \W]\$ '

if [[ -e /usr/share/bash-completion/bash_completion ]]; then
	. /usr/share/bash-completion/bash_completion
fi

### Bash Settings ###
shopt -s cmdhist        # save multi-line commands in history as single line
shopt -s histappend     # do not overwrite history
shopt -s expand_aliases # expand aliases
shopt -s histverify
shopt -s dotglob
bind "set completion-ignore-case on"

### Export ###
export HISTCONTROL=ignoreboth:erasedups
export PAGER='most'
export VISUAL=nvim
export EDITOR=nvim
export QT_STYLE_OVERRIDE=kvantum

if [[ -e $HOME/.android_sdk/cmdline-tools/latest/bin/sdkmanager ]]; then
	PATH="$HOME/.android_sdk/platform-tools:$HOME/.android_sdk/cmdline-tools/latest/bin:$HOME/.android_sdk/ndk-bundle:$HOME/.android_sdk/build-tools/33.0.1:$PATH"
	export ANDROID_HOME=~/.android_sdk
	export ANDROID_SDK_ROOT=~/.android_sdk
	export ANDROID_NDK_HOME=~/.android_sdk/ndk-bundle
fi

if [[ -e /usr/bin/paru ]]; then
	export PARU_PAGER=most
fi

if [[ -d ~/.cargo/bin ]]; then
	PATH="$PATH:$HOME/.cargo/bin:$HOME/.cargo/env"
fi

#if [[ -d "$HOME/.bin" ]]; then
PATH="$HOME/.bin:$PATH"
#fi
#if [[ -d "$HOME/.local/bin" ]]; then
#	PATH="$HOME/.local/bin:$PATH"
#fi

if [[ -d "/home/gjbigs/.bin" ]]; then
	PATH="/home/gjbigs/.bin:$PATH"
fi
if [[ -d "/home/gjbigs/.local/bin" ]]; then
	PATH="/home/gjbigs/.local/bin:$PATH"
fi

### Aliases & Functions ###
alias ag='ag --nonumbers --hidden'
alias rg='rg --color always --heading -N'
alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias rmlogoutlock="sudo rm /tmp/arcologout.lock"
alias merge="xrdb -merge ~/.Xresources"
cleanup() {
	declare porph
	porph="$(pacman -Qtdq | sudo pacman -Rns -)"
	$porph
}
ex() {
	if [ -f $1 ]; then
		case $1 in
		*.tar.bz2) tar xjf $1 ;;
		*.tar.gz) tar xzf $1 ;;
		*.bz2) bunzip2 $1 ;;
		*.rar) unrar x $1 ;;
		*.gz) gunzip $1 ;;
		*.tar) tar xf $1 ;;
		*.tbz2) tar xjf $1 ;;
		*.tgz) tar xzf $1 ;;
		*.zip) unzip $1 ;;
		*.Z) uncompress $1 ;;
		*.7z) 7z x $1 ;;
		*.deb) ar x $1 ;;
		*.tar.xz) tar xf $1 ;;
		*.tar.zst) tar xf $1 ;;
		*) echo "'$1' cannot be extracted via ex()" ;;
		esac
	else
		echo "'$1' is not a valid file"
	fi
}
complete -cf rdo
complete -cf doas
setnvid() {
	alias vid='neovide-lunarvim'
	alias svid='sudo /home/gjbigs/.local/bin/neovide-lunarvim'
}
if [[ -e /home/gjbigs/.local/bin/neovide-lunarvim ]]; then
	setnvid
fi
setlvim() {
	alias vi='lvim'
	alias vim='lvim'
	alias svi='sudo /usr/bin/lvim'
	export EDITOR='lvim'
	export VISUAL='lvim'
}
if [[ -e /usr/bin/lvim ]]; then
	setlvim
else
	alias vi='nvim'
	alias vi='nvim'
	alias svi='sudo /usr/bin/nvim'
fi
alias lnsf='ln -sf'
alias lnf='ln -f'
alias sct='sudo systemctl'
alias jctl='journalctl -exu'
alias sno='sudo /usr/bin/nano'
alias ls='exa --icons --color=auto'
alias ll='exa -l --octal-permissions --icons --color=auto'
alias la='exa -a --icons --color=auto'
alias lla='exa -la --octal-permissions --icons --color=auto'
alias lsd='exa -d --icons --color=auto'
alias lld='exa -ld --octal-permissions --icons --color=auto'
alias syncmirrors="sudo reflector -f 50 -c US -p https --sort rate --save /etc/pacman.d/mirrorlist"
if [[ -e /usr/bin/doas ]]; then
	alias sudo='doas '
else
	#	alias sudo='sudo -v; sudo '
	alias sudo='sudo '
fi
alias rdo='rdo '
alias kpi='killall picom'
alias jo='joshuto'
alias diff='diff --color -y'
## NVM ##
snvm() {
	if [[ -e /usr/share/nvm/init-nvm.sh ]]; then
		source /usr/share/nvm/init-nvm.sh
	fi
}

ssudo() {
	[[ "$(type -t "$1")" = "function" ]] &&
		ARGS="$@" && sudo bash -c "$(declare -f "$1"); $ARGS"
}

alias ssudo="ssudo "

. /home/gjbigs/linux/functions.sh

### Fetch ###
if [[ "$(id -u)" -ne 0 ]] && [[ -e /bin/fastfetch ]]; then
	sleep 0.05
	fastfetch
fi
