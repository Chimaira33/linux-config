#!/bin/bash

cdpkg() {
	cd /home/pkgs_gjbigs || return
}

swapset() {
	PS3="My selection is:"

	select SELECT in 133 10 cancel; do
		case $SELECT in
		133)
			ctl vm.swappiness 175
			return
			;;
		10)
			ctl vm.swappiness 10
			return
			;;
		cancel)
			pln "Cancel and Exit."
			return
			;;
		*)
			pln "Please make a selection from the provided options."
			;;
		esac
	done
}

mnt() {
	sudo mount "$@"
}

umnt() {
	sudo umount "$1"
}

adgh() {
	if [[ -e /opt/AdGuardHome/AdGuardHome ]]; then
		sudo /opt/AdGuardHome/AdGuardHome -s "$1"
	else
		return
	fi
}

cset() {
	if [[ "$(id -u)" -eq 0 ]]; then
		chattr +i "$1"
	else
		sudo chattr +i "$1"
	fi
}

crem() {
	if [[ "$(id -u)" -eq 0 ]]; then
		chattr -i "$1"
	else
		sudo chattr -i "$1"
	fi
}

zen() {
	cwr() {
		declare S
		S=$(pln "$1" | bc)
		declare P
		P=$(pln ${S%.*})
		wr $P "$2"
	}
	declare lat=4
	declare min=1
	declare idlemin=0.5
	declare wake=1
	declare mig=2
	declare MOD=4000000
	for I in /sys/kernel/debug/sched; do
		wr 128 "$I"/nr_migrate
		cwr "$lat * $MOD" >"$I"/latency_ns
		cwr "$min * $MOD" >"$I"/min_granularity_ns
		cwr "$idlemin * $MOD" >"$I"/idle_min_granularity_ns
		cwr "$wake * $MOD" >"$I"/wakeup_granularity_ns
		cwr "$mig * $MOD" >"$I"/migration_cost_ns
	done
	ctl kernel.sched_cfs_bandwidth_slice_us 3000
}

calc() {
	declare S
	S=$(pln "$1" | bc)
	pln "${S%.*}"
}

dcalc() {
	declare CLC
	CLC=$(pln "$1" | bc -l)
	printf "%.3f\n" "$CLC"
}

disidle() {
	sudo cpupower -c all idle-set -D 70
}

rnice() {
	if [[ "$(id -u)" -eq 0 ]]; then
		declare rni
		rni=("renice -n $1")
	else
		declare rni
		rni=("sudo renice -n $1")
	fi
	$rni "$(pgrep "$2")"
}

boost() {
	sudo /usr/lib/booster/regenerate_images
}

reshiny() {
	if [[ "$(id -u)" -eq 0 ]]; then
		declare shi=("shiny-mirrors refresh -f medium -M transfer")
	else
		declare shi=("sudo shiny-mirrors refresh -f medium -M transfer")
	fi
	$shi
}

visedit() {
	if [[ "$(id -u)" -eq 0 ]]; then
		declare vdo=("visudo -f /etc/sudoers.d/01-gb")
	else
		declare vdo=("sudo visudo -f /etc/sudoers.d/01-gb")
	fi
	$vdo
}

vsdo() {
	if [[ "$(id -u)" -eq 0 ]]; then
		declare vsd=("visudo")
	else
		declare vsd=("sudo visudo")
	fi
	$vsd
}

ytd() {
	yt-dlp "$1" -f "bv*[height<=?1080][vcodec~='^((he|a)vc|h26[45])']+ba/b"
}

ffd() {
	fd -u -t f "$@"
}

dfd() {
	fd -u -t d "$@"
}

rpsync() {
	repo sync "$1" --force-sync \
		--no-tags \
		--no-clone-bundle
}

mkgrub() {
	if [[ "$(id -u)" -eq 0 ]]; then
		declare mkg=("grub-mkconfig -o /boot/grub/grub.cfg")
	else
		declare mkg=("sudo grub-mkconfig -o /boot/grub/grub.cfg")
	fi
	$mkg
}

rsy() {
	rsync -rpElog "$@"
}

configbackup() {
	rssy() {
		rsync -rpEl "$@"
	}
	srssy() {
		sudo rsync -rpEl "$@"
	}
	declare GB="/home/gjbigs/.local/share/yadm/"
	declare GH="/home/gjbigs"
	declare BK="$GH/linux"
	declare CG="$GH/.config"
	declare BG="$BK/.config"
	declare ET="/etc"
	declare BT="$BK/etc"
	declare LVC1="/home/gjbigs/.config/lvim/config.lua"
	declare LVC2="/home/gjbigs/linux/.config/lvim/config.lua"
	declare LVI1="/home/gjbigs/.local/share/lunarvim/lvim/init.lua"
	declare LVI2="/home/gjbigs/linux/.local/share/lunarvim/lvim/init.lua"
	rsy -b -v --backup-dir=.backup $GH/.gnupg/dirmngr.conf $BK/.gnupg/
	rsy -b -v --backup-dir=.backup $GH/.gnupg/gpg.conf $BK/.gnupg/
	rsy -b -v --backup-dir=.backup $GH/.xmonad/xmonad.hs $BK/.xmonad/
	rsy -b -v --backup-dir=.backup $GH/.xmonad/scripts $BK/.xmonad/
	rsy -b -v --backup-dir=.backup $GH/.gtkrc-2.0 $BK/
	rsy -b -v --backup-dir=.backup $GH/.bashrc $BK/
	rsy -b -v --backup-dir=.backup $GH/functions.sh $BK/
	rsy -b -v --backup-dir=.backup $GH/boot.sh $BK/
	rsy -b -v --backup-dir=.backup $GH/zram.sh $BK/
	rsy -b -v --backup-dir=.backup $GH/.xsettingsd $BK/
	rsy -b -v --backup-dir=.backup $GH/.Xresources $BK/
	rsy -b -v --backup-dir=.backup $GH/.profile $BK/
	rsy -b -v --backup-dir=.backup $CG/qtile $BG/
	rsy -b -v --backup-dir=.backup $CG/alacritty/alacritty.yml $BG/alacritty/
	rsy -b -v --backup-dir=.backup $CG/chromium/ $BG/chromium/
	rsy -b -v --backup-dir=.backup $CG/gtk-3.0 $BG/
	rsy -b -v --backup-dir=.backup $CG/gtk-4.0 $BG/
	rsy -b -v --backup-dir=.backup $CG/joshuto $BG/
	rsy -b -v --backup-dir=.backup $CG/paru $BG/
	rsy -b -v --backup-dir=.backup $CG/polybar $BG/
	rsy -b -v --backup-dir=.backup $CG/qt5ct $BG/
	rsy -b -v --backup-dir=.backup $CG/qt6ct $BG/
	rsy -b -v --backup-dir=.backup $CG/spacefm $BG/
	rsy -b -v --backup-dir=.backup $CG/xfce4 $BG/
	rsy -b -v --backup-dir=.backup $CG/chromium-flags.conf $BG/
	rsy -b -v --backup-dir=.backup $LVC1 $LVC2
	rsy -b -v --backup-dir=.backup $LVI1 $LVI2
	rssy -b -v --backup-dir=.backup $ET/ananicy.d $BT/
	srssy -b -v --backup-dir=.backup $ET/sudoers.d $BT/
	rssy -b -v --backup-dir=.backup $ET/mkinitcpio.d/ $BT/
	rssy -b -v --backup-dir=.backup $ET/sysctl.d $BT/
	rssy -b -v --backup-dir=.backup $ET/kernel/clear_cmdline $BT/kernel/
	rssy -b -v --backup-dir=.backup $ET/kernel/cmdline $BT/kernel/
	rssy -b -v --backup-dir=.backup $ET/makepkg.conf $BT/
	rssy -b -v --backup-dir=.backup $ET/o3_makepkg.conf $BT/
	rssy -b -v --backup-dir=.backup $ET/ofast_makepkg.conf $BT/
	rssy -b -v --backup-dir=.backup $ET/nftables.conf $BT/
	rssy -b -v --backup-dir=.backup $ET/pacman.conf $BT/
	rssy -b -v --backup-dir=.backup $ET/profile $BT/
	rssy -b -v --backup-dir=.backup $ET/environment $BT/
	rssy -b -v --backup-dir=.backup $ET/powerdns $BT/
	rssy -b -v --backup-dir=.backup $ET/security/faillock.conf $BT/security/

}

crsync() {
	declare GH="/home/gjbigs"
	declare RH="/root"
	rrsync() {
		sudo rsync -rpEl \
			--chown=root:root "$@"
	}
	rdrsync() {
		sudo rsync -rpEl \
			--chown=root:root \
			--delete-after "$1" "$2"
	}
	echo "Running Sync"
	rrsync "$GH"/.config/ "$RH"/.config/
	rdrsync "$GH"/.local/share/lunarvim/ \
		"$RH"/.local/share/lunarvim/
	rrsync "$GH"/.gtkrc-2.0 \
		"$GH"/.profile \
		"$GH"/.Xresources \
		"$GH"/.xsettingsd "$RH"/
	echo "Linking .bashrc"
	sudo ln -f "$GH"/.bashrc "$RH"/.bashrc
}

pacexp() {
	if [[ "$(id -u)" -eq 0 ]]; then
		declare setex=("pacman -D --asexplicit")
	else
		declare setex=("sudo pacman -D --asexplicit")
	fi
	$setex "$@"
}

clearboot() {
	declare mkin=("sudo mkinitcpio -p linux-clear -- --uefi /boot/efi/EFI/Linux/clear.efi")
	$mkin
}

xanmodboot() {
	declare mkin=("sudo mkinitcpio -p linux-xanmod -- --uefi /boot/efi/EFI/Linux/xanmod.efi")
	$mkin
}

zenboot() {
	declare mkin=("sudo mkinitcpio -p linux-zen -- --uefi /boot/efi/EFI/Linux/zen.efi")
	$mkin
}

scat() {
	declare scatt=("sudo cat")
	$scatt "$1"
}

rmd() {
	declare rmd=("rm -rf")
	$rmd "$@"
}

schedb() {
	declare schb=("schedtool -B -n 1 -e ionice -n 1")
	$schb "$@"
}

bootupdate() {
	declare sucp=("sudo cp")
	$sucp /boot/*.img \
		/efi/
	$sucp /boot/vmlinuz-linux \
		/efi/
}

mkinit() {
	declare sumkin=("sudo mkinitcpio -p")
	declare mkin=("mkinitcpio -p")
	if [[ "$(id -u)" -eq 0 ]]; then
		$mkin "$1"
	else
		$sumkin "$1"
	fi
}

delpaclk() {
	declare surm=("sudo rm")
	if [[ -e /var/lib/pacman/db.lck ]]; then
		$surm /var/lib/pacman/db.lck
	fi
}

pln() {
	printf '%s\n' "$*"
}

wr() {
	if [[ ! -e "$2" ]]; then
		pln "$2 Doesn't Exist. Skipping."
		return 1
	fi
	declare chk
	chk="$(cat "$2" </dev/null 2>/dev/null)"
	if [[ $chk = "$1" ]]; then
		pln "$2 is already set to $1. Skipping."
		return 1
	fi
	if ! pln "$1" >"$2" </dev/null 2>/dev/null; then
		pln "Error When Writing To $2."
		return 1
	fi
}

paci() {
	declare spcu=("sudo pacman -U")
	declare pcu=("pacman -U")
	if [[ "$(id -u)" -eq 0 ]]; then
		$pcu *.zst
		#$pcu *.bz2
	else
		$spcu *.zst
	fi
}

pstper() {
	declare per=("sudo pstate-frequency -S -g performance")
	$per
}

pstpow() {
	declare pow=("sudo pstate-frequency -S -g powersave")
	$pow
}

mkpkg() {
	# declare mkg=("schedtool -B -n 1 -e ionice -n 1 makepkg -sc --needed --noconfirm")
	declare mkg=("makepkg -sc --needed --noconfirm")
	if [[ "$(id -u)" -eq 0 ]]; then
		pln "Command can't be run as root. Exiting"
		return 1
	else
		$mkg
	fi
}

o3mkpkg() {
	declare mkg=("makepkg -sc --needed --noconfirm --config /etc/o3_makepkg.conf")
	$mkg "$@"
}

ofmkpkg() {
	declare mkg=("makepkg -sc --needed --noconfirm --config /etc/ofast_makepkg.conf")
	$mkg
}

qdisc() {
	#declare tcset=("tc qdisc replace root dev enp3s0 fq_codel")
	#declare tcset2=("tc qdisc replace root dev enp3s0 fq_pie")
	declare tcset3=("tc qdisc replace root dev enp3s0 fq")
	$tcset3
	#$tcset
}

ctl() {
	declare curval
	curval="$(sysctl -e -n "$1")"
	if [[ -z $curval ]]; then
		pln "Key $1 does not exist. Skipping."
		return 1
	fi
	if [[ $curval == "$2" ]]; then
		pln "$1 is already set to $2. Skipping."
		return 0
	fi
	if [[ "$(id -u)" -eq 0 ]]; then
		declare wctl
		wctl=("sysctl -q -w")
	else
		declare wctl
		wctl=("sudo sysctl -q -w")
	fi
	$wctl "$1"="$2"
}

aurs() {
	declare auracs=("auracle --sort=votes search")
	$auracs "$@"
}

aurc() {
	declare aurac=("auracle -C /home/pkgs_gjbigs -r clone")
	declare cdpkg=("cd /home/pkgs_gjbigs")
	$aurac "$1"
	$cdpkg/$1
}

prepo() {
	declare pac=("paru --repo -G")
	declare cdpkg=("cd /home/pkgs_gjbigs")
	$cdpkg
	$pac "$1"
	$cdpkg/"$1"
}

gits() {
	git status
}

gcp() {
	git cherry-pick "$1"
}

gcpm() {
	git cherry-pick -m 1 "$1"
}

gita() {
	git remote add "$@"
}

gitf() {
	git fetch "$@"
}

gitcm() {
	git commit -m "$1"
}

gitc() {
	# declare cdpkg=("cd /home/pkgs_gjbigs")
	declare ggitc=("git clone")
	# declare exll=("exa -l --color=auto")
	# declare cdba=("cd ./")
	# $cdpkg
	$ggitc "$@"
	# $cdba"$2"
	# $exll
}

sbash() {
	if [[ ! -e "$1" ]]; then
		pln "Doesn't Exist."
		return 1
	else
		sudo bash "$1"
	fi
}

ssudo() {
	[[ "$(type -t "$1")" = "function" ]] &&
		ARGS="$@" && sudo bash -c "$(declare -f "$1"); $ARGS"
}

alias ssudo="ssudo "
